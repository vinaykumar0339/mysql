CREATE TABLE prescription (
    prescription_id INT NOT NULL AUTO_INCREMENT,
    drug VARCHAR(30),
    date DATE,
    dosage VARCHAR(20),
    PRIMARY KEY (prescription_id)
);

CREATE TABLE patient (
    patient_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30),
    dob DATE,
    address VARCHAR(255),
    PRIMARY KEY (patient_id)
);

CREATE TABLE doctor (
    doctor_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30),
    secretary VARCHAR(30),
    patient_id INT,
    prescription_id INT,
    FOREIGN KEY (patient_id) REFERENCES patient(patient_id),
    FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id),
    PRIMARY KEY (doctor_id)
);