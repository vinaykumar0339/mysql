CREATE TABLE branch {
    branch_id INT NOT NULL AUTO_INCREMENT,
    branch_addr VARCHAR(255),
    isbn VARCHAR(50),
    title VARCHAR(30),
    author VARCHAR(20),
    publisher VARCHAR(50),
    num_copies INT DEFAULT 0,
    PRIMARY KEY (branch_id)
}