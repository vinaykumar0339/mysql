CREATE TABLE manager (
    manager_id INT NOT NULL AUTO_INCREMENT,
    manager_name VARCHAR(50),
    manager_location VARCHAR(50),
    PRIMARY KEY (manager_id)
);

CREATE TABLE contract (
    contract_id INT NOT NULL AUTO_INCREMENT,
    estimated_cost INT,
    completion_date DATE,
    PRIMARY KEY (contract_id)
);

CREATE TABLE staff (
    staff_id INT NOT NULL AUTO_INCREMENT,
    staff_name VARCHAR(50),
    staff_location VARCHAR(50),
    PRIMARY KEY (staff_id)
);

CREATE TABLE client (
    client_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    location VARCHAR(50),
    manager_id INT,
    contract_id INT,
    staff_id INT,
    FOREIGN KEY (manager_id) REFERENCES manager(manager_id),
    FOREIGN KEY (contract_id) REFERENCES contract(contract_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    PRIMARY KEY (client_id)
);